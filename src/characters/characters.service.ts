import { Injectable } from '@nestjs/common';
import { characters } from '../../static/characters.js'


const _sampleCharacter = characters[0];
const _characterAttributes = Object.keys(_sampleCharacter);


@Injectable()
export class CharactersService {
  getCharacters = () => characters;

  getCharacterAttributes = () => {
    const attributesTypes = {};

    _characterAttributes.forEach((attribute) => {
      if (Array.isArray(_sampleCharacter[attribute])) {
        return attributesTypes[attribute] = ["string"];
      }

      return attributesTypes[attribute] = typeof _sampleCharacter[attribute];
    });

    return attributesTypes;
  };


  getRandomCharacter = () => {
    return (characters[(Math.floor(Math.random() * characters.length))]);
  };


  getRandomCharacterByAttribute = ({ attribute, value }) => {
    if (!_characterAttributes.includes(attribute)) {
      return null;
    }

    const matches = characters.filter((character) => {
      return character[attribute].toLowerCase().includes(value.toLowerCase());
    });

    return (
      matches.length > 0
        ? matches[(Math.floor(Math.random() * matches.length))]
        : {}
    );
  };


  freeTextSearch = ({ text }) => {
    const matchingCharacters = characters.filter((character) => {
      const characterValuesFlatArr = Object.values(character)['flat'](Infinity);
      for (const value of characterValuesFlatArr) {
        if (value.length && value.toLowerCase().includes(text.toLowerCase())) {
          return true;
        };
      }
    });

    return matchingCharacters;
  };
};
