import { Controller, Get, Query } from '@nestjs/common';
import { CharactersService } from './characters.service';


@Controller('/characters')
export class CharactersController {
  constructor(private readonly charactersService: CharactersService) { };

  // GET - '/characters' => all characters
  @Get()
  getCharacters(): any {
    const characters = this.charactersService.getCharacters();
    return { data: characters };
  };


  // GET - '/characters/attributes' => character attributes
  @Get('attributes')
  getCharacterAttributes(): any {
    const characterAttributes = this.charactersService.getCharacterAttributes();
    return { data: characterAttributes };
  };


  // GET - '/characters/random' => random character
  @Get('random')
  getRandomCharacter(): any {
    const character = this.charactersService.getRandomCharacter();
    return { data: character };
  };


  /*
   GET - '/characters/random/search?attribute=value' 
   => random character by value of queried attribute
   */
  @Get('random/search?')
  getRandomCharacterByAttribute(@Query() query: Record<string, unknown>): any {
    const [attribute, value] = Object.entries(query)[0];
    const character = this.charactersService.getRandomCharacterByAttribute({
      attribute,
      value
    });
    if (character === null) {
      return {
        statusCode: 400,
        message: `Invalid query key: ${attribute.toLocaleUpperCase()}`
      };
    }

    return { data: character };
  };


  // GET - '/characters/search?text=' => character(s) by free text search match
  @Get('search?')
  getCharacterFreeText(@Query() query: Record<string, unknown>): any {
    const { text } = query;
    if (!text) {
      return {
        statusCode: 400,
        message: `Invalid query key. Expects 'text' as query key`
      };
    }

    const matches = this.charactersService.freeTextSearch({ text });

    return { data: matches };
  };
};
