# VAUBAN Back End Test Solution - Louis Foged

## Stack

- Node.js with NestJS framework

## Description and Requirements

API that retrieves Star Wars character(s) in JSON format.

- Retrieve a random character from a given category.
- Retrieve a list of available categories.
- Free text search.
- Provide an API documentation that is testable on Postman.

## Solution Overview

- Relevant controllers and services are located in ```src/characters``` directory.
- API documentation for testing with Postman is located in ```postman_API_test.postman_collection``` file.
  - Open file in postman to see documentation of API endpoints

## API Endpoints
- GET: http://localhost:3000/
  -  returns 'Hello World'
- GET: http://localhost:3000/characters
  - returns all characters
- GET: http://localhost:3000/characters/attributes
  - returns character attribute (category) description
- GET: http://localhost:3000/characters/random
  - returns a random character
- GET: http://localhost:3000/characters/random/search?eye_color=blue
  - returns random character with a given attribute (from a given category). Supports incomplete match (eg: 'name=lu' will return character 'Luke Skywalker')
- GET: http://localhost:3000/characters/search?text=brown
  - returns characters that have values that contain ```text``` query value. Supports incomplete match (eg: 'text=bl' will return characters that have values of 'blue', 'black', 'blond', etc.)

##### Author: Louis Foged